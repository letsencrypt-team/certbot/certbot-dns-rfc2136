python-certbot-dns-rfc2136 (2.9.0-1) unstable; urgency=medium

  * New upstream version 2.9.0

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Fri, 16 Feb 2024 15:36:38 -0500

python-certbot-dns-rfc2136 (2.1.0-1) unstable; urgency=medium

  * New signing key.
  * New upstream version 2.1.0
  * Bump dependency requirements, make dynamic

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Wed, 07 Dec 2022 21:38:26 -0500

python-certbot-dns-rfc2136 (1.21.0-1) unstable; urgency=medium

  [ Debian Janitor ]

  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Harlan Lieberman-Berg ]
  * Add new signing keys, see bug #999503
  * New upstream version 1.21.0
  * Bump deps.
  * Bump S-V; no changes needed
  * Bump copyright year

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Sun, 21 Nov 2021 15:12:54 -0500

python-certbot-dns-rfc2136 (1.10.1-1) unstable; urgency=medium

  * New upstream version 1.10.1
  * Bump S-V; no changes needed.
  * Bump dh to 13.

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Fri, 04 Dec 2020 22:43:50 -0500

python-certbot-dns-rfc2136 (1.3.0-1) unstable; urgency=medium

  * New upstream version 1.3.0
  * Switch to Provides based ABI management
  * Add dep on pytest
  * Bump S-V, switching to debhelper-compat

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Mon, 27 Apr 2020 20:10:32 -0400

python-certbot-dns-rfc2136 (0.35.1-1) unstable; urgency=medium

  * New upstream version 0.35.1
  * Bump acme, certbot dependency versions
  * Add R-R-R; bump S-V

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Tue, 09 Jul 2019 19:45:21 -0400

python-certbot-dns-rfc2136 (0.24.0-2) unstable; urgency=medium

  * Update maintainer email to tracker.d.o.  (Closes: #899879)

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Thu, 31 May 2018 21:49:04 -0400

python-certbot-dns-rfc2136 (0.24.0-1) unstable; urgency=medium

  * New upstream version 0.24.0
  * Bump S-V; no changes needed.

 -- Harlan Lieberman-Berg <hlieberman@debian.org>  Thu, 03 May 2018 20:00:52 -0400

python-certbot-dns-rfc2136 (0.23.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control: Add an Enhances on certbot.

 -- Andrew Starr-Bochicchio <asb@debian.org>  Sat, 07 Apr 2018 16:14:55 -0400

python-certbot-dns-rfc2136 (0.22.0-1) unstable; urgency=medium

  * Initial release (Closes: #893307).

 -- Andrew Starr-Bochicchio <asb@debian.org>  Sat, 17 Mar 2018 17:45:07 -0400
